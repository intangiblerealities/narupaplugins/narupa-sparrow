import pytest
from ase import Atoms
from scine_sparrow import calculate_gradients, calculate_energy
import numpy as np

@pytest.fixture
def co_atoms() -> Atoms:
    d = 1.1
    co = Atoms('CO', positions=[(0, 0, 0), (0, 0, d)],
               cell=[2, 2, 2],
               pbc=[1, 1, 1])
    return co


def test_energy(co_atoms):
    energy = calculate_energy(co_atoms.get_chemical_symbols(), co_atoms.positions, method='PM6')
    assert energy == pytest.approx(-15.171055082816595)


def test_gradients(co_atoms):
    gradients = calculate_gradients(co_atoms.get_chemical_symbols(), co_atoms.positions, method='PM6')
    expected_gradients = [[1.1511114191772035e-16, -2.9536552354054594e-17, 0.09145123302131775],
                          [-1.1511114191772035e-16, 2.9536552354054594e-17, -0.09145123302131775]]
    assert np.allclose(gradients, expected_gradients)