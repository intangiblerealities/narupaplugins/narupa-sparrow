# Copyright (c) Intangible Realities Lab, University Of Bristol. All rights reserved.
# Licensed under the GPL. See License.txt in the project root for license information.
"""
Demonstrates interactive molecular dynamics running with Sparrow as the engine and ASE as the integrator.

Run with:

.. code bash
    python cli.py structure.xyz

"""
import argparse
import textwrap

from ase import units, io
from ase.md import MDLogger, Langevin
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution
from narupa.ase import NarupaASEDynamics, TrajectoryLogger

from narupa.sparrow.sparrow_calculator import SparrowCalculator


def handle_user_arguments() -> argparse.Namespace:
    """
    Parse the arguments from the command line.

    :return: The namespace of arguments read from the command line.
    """
    description = textwrap.dedent("""\
    Run an ASE IMD simulation from an ASE supported file using Sparrow.
    """)
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument(
        'structure_path',
        help='The structure to run in a format supported by ASE.',
    )
    parser.add_argument(
        '-v', '--verbose',
        action='store_true',
        help='Display state information.',
    )
    parser.add_argument('-p', '--port', default=None)
    parser.add_argument('-a', '--address', default=None)
    parser.add_argument('-f', '--frame_interval', type=int, default=1)
    parser.add_argument('-t', '--temperature', type=float, default=300)
    parser.add_argument('-g', '--friction_coefficient', type=float, default=1e-04)
    parser.add_argument('-s', '--time-step', type=float, default=0.5)
    parser.add_argument('-m', '--method', default='DFTB0')
    parser.add_argument(
        '-o', '--trajectory-file', type=str, default=None,
        help='Base filename for the trajectory output file. A timestamp will '
             'be inserted between the name and file extensions. Can be any '
             'file that ASE can output in append mode, such as XYZ.'
    )
    parser.add_argument(
        '--write-interval', type=int, default=1,
        help='Write a trajectory frame to file every WRITE_INTERVAL dynamics '
             'steps.',
    )

    arguments = parser.parse_args()
    return arguments


def attach_logging(imd: NarupaASEDynamics, output_path, interval):
    """
    Attaches trajectory logging to the given Narupa iMD runner.

    Args:
        imd: Narupa iMD runner
        output_path: Directory to output to
        interval: Interval at which to write.

    """
    logger = TrajectoryLogger(imd.atoms, output_path)
    imd.on_reset_listeners.append(logger.reset)
    imd.dynamics.attach(logger, interval)


def run_imd(arguments) -> NarupaASEDynamics:
    atoms = io.formats.read(arguments.structure_path)

    atoms.set_calculator(SparrowCalculator(method=arguments.method))

    print(f'ASE energy: {atoms.get_potential_energy()}')
    # Set the momenta corresponding to temperature
    MaxwellBoltzmannDistribution(atoms, arguments.temperature * units.kB)

    dyn = Langevin(atoms,
                   arguments.time_step * units.fs,
                   arguments.temperature * units.kB,
                   arguments.friction_coefficient)

    if arguments.verbose:
        dyn.attach(MDLogger(dyn, atoms, '-', header=True, stress=False,
                            peratom=False), interval=100)

    # sets up the narupa server, multiplayer and discovery.
    with NarupaASEDynamics.basic_imd(dyn, arguments.address, arguments.port) as imd:
        # sets up trajectory logging to file
        if arguments.trajectory_file is not None:
            attach_logging(imd, arguments.trajectory_file, arguments.write_interval)
        while True:
            imd.run(10)


def main():
    """
    Entry point for the command line.
    """
    arguments = handle_user_arguments()

    run_imd(arguments)


if __name__ == '__main__':
    main()
