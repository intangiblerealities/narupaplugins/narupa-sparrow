# Narupa Sparrow 

Provides an integration between SCINE Sparrow and Narupa 2, providing 
interactive molecular dynamics with semi-empirical methods.

## Getting Started

Prequisites: 

* An installation of [narupa-protocol](https://gitlab.com/intangiblerealities/narupa-protocol). 
* An installation of [SCINE Sparrow](https://github.com/qcscine/sparrow)

If SCINE Sparrow is not installed in a default system location, be 
sure to add the lib paths to LD_LIBRARY_PATH (Unix), 
DYLD_LIBRARY_PATH (Mac OS X) or PATH (Windows) as appropriate.
Additionally, add the lib path to PYTHON_PATH, and set the 
SCINE_MODULE_PATH.

```bash
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/path/to/sparrow/lib
export PYTHONPATH=${PYTHONPATH}:/path/to/sparrow/lib
export SCINE_MODULE_PATH=/path/to/sparrow/lib
``` 

### Installation 

clone the repository, open a terminal and navigate
to the root. 

```
pip install .
```

### Running the Server 

Installing the module into your python environment gives you
access to the command line interface `narupa-sparrow`, which serves
as a basic example of running an IMD server.

```bash
narupa-sparrow examples/ethane.xyz
```

To see the available options, run 
```bash
narupa-sparrow -h
```

### Sparrow Installation Troubleshooting 

#### Mac OS X

At the time of writing, it was necessary to use gcc rather
than clang to install SCINE Sparrow. 

```bash
brew install gcc
brew install boost CC=/usr/local/gcc-9
brew install eigen
```

Then, assuming a build directory called `build`, compile with:

```bash
cmake -DCMAKE_C_COMPILER=/usr/local/bin/gcc-9 -DCMAKE_CXX_COMPILER=/usr/local/bin/g++-9 -DSCINE_BUILD_PYTHON_BINDINGS=ON -DCMAKE_INSTALL_PREFIX=../install ..
```

Replace flags as appropriate. 
to be moved to successfully link `sparrow`.